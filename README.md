---
output: github_document
---
[![pipeline status](https://gitlab.com/fvafrcu/regulaFalsi/badges/master/pipeline.svg)](https://gitlab.com/fvafrcu/regulaFalsi/commits/master)    
[![coverage report](https://gitlab.com/fvafrcu/regulaFalsi/badges/master/coverage.svg)](https://gitlab.com/fvafrcu/regulaFalsi/commits/master)
<!-- 
    [![Build Status](https://travis-ci.org/fvafrcu/regulaFalsi.svg?branch=master)](https://travis-ci.org/fvafrcu/regulaFalsi)
    [![Coverage Status](https://codecov.io/github/fvafrcu/regulaFalsi/coverage.svg?branch=master)](https://codecov.io/github/fvafrcu/regulaFalsi?branch=master)
-->
[![CRAN_Status_Badge](https://www.r-pkg.org/badges/version/regulaFalsi)](https://cran.r-project.org/package=regulaFalsi)
[![RStudio_downloads_monthly](https://cranlogs.r-pkg.org/badges/regulaFalsi)](https://cran.r-project.org/package=regulaFalsi)
[![RStudio_downloads_total](https://cranlogs.r-pkg.org/badges/grand-total/regulaFalsi)](https://cran.r-project.org/package=regulaFalsi)

<!-- README.md is generated from README.Rmd. Please edit that file -->



# regulaFalsi
## Introduction
Please read the
[vignette](https://gitlab.com/fvafrcu/regulaFalsi/inst/doc/An_Introduction_to_regulaFalsi.html).

Or, after installation, the help page:

```r
help("regulaFalsi-package", package = "regulaFalsi")
```

```
#> Find Function Roots with the Double False Position Method
#> 
#> Description:
#> 
#>      Find a root for a univariate function within a given interval
#>      using the double false position method. See <URL:
#>      https://en.wikipedia.org/wiki/False_position_method> (last checked
#>      on Fri Oct 5 14:47:50 CEST 2018) or
#>      Fibonacci (1202): Liber Abaci for details.
#> 
#> Details:
#> 
#>      Check
#>      'vignette("An_Introduction_to_regulaFalsi", package =
#>      "regulaFalsi")' also.
```

## Installation

You can install regulaFalsi from github with:


```r
if (! require("devtools")) install.packages("devtools")
devtools::install_git("https://gitlab.com/fvafrcu/regulaFalsi")
```


