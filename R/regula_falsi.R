.regula_falsi <- function(f, a, b, tolerance = .Machine[["double.eps"]]) {
    is_not_zero <- TRUE
    while (is_not_zero) {
        c <- (a * f(b) - b * f(a)) / (f(b) - f(a))
        is_not_zero <- (abs(f(c)) > tolerance)
        if (f(c) * f(a) > 0) {
            a <- c
        } else {
            b <- c
        }
    }
    return(c)
}

.m <- function(f2, fz, improvement) {
    m <- switch(improvement,
                pegasus = f2 / (f2 + fz),
                illinois = 0.5,
                anderson_bjoerck = {
                    r <- 1 - fz / f2
                    if (r >= 0) r else 0.5
                }
           )
    return(m)

}

.better_false_position <- function(x1, x2, f1, f2, f, epsx, epsf, improvement) {
    result <- NULL
    z <- x1 - f1 * (x2 - x1) / (f2 - f1)
    fz  <-  f(z)

    if (abs(x2 - x1) < epsx || abs(fz) < epsf) {
        result <- z
    } else {
        if (fz * f2 < 0) {
            result <- .better_false_position(x2, z, f2, fz, f, epsx, epsf,
                                            improvement)
        } else {
            m_f1 <- .m(f2, fz, improvement) * f1
            result <- .better_false_position(x1, z, m_f1, fz, f, epsx, epsf,
                                            improvement)
        }
    }
    return(result)
}

#' \verb{Regula Duarum Falsarum Positionum (Regula Falsi)}
#'
#' Find a function's root within an interval.
#'
#' @param f The function.
#' @param x1 Lower boundary of the interval.
#' @param x2 Upper boundary of the interval.
#' @param epsx Tolerance in x.
#' @param epsf Tolerance in f(x).
#' @param improvement The improvement to be applied to \verb{regula falsi}.
#' @return An approximated value of x where f(x) = 0.
#' @seealso \code{\link[stats:uniroot]{stats::uniroot}},
#' \code{\link[pracma:regulaFalsi]{pracma::regulaFalsi}},
#' \code{help("regulaFalsi-package", package = "regulaFalsi")}.
#' @export
#' @examples
#' expectation <- 0.865474033101614
#' # expectation taken from
#' # https://en.wikipedia.org/wiki/False_position_method#Example_code
#' # on Fri Oct  5 14:56:33 CEST 2018
#' result <- regulaFalsi::regula_falsi(f = function(x) return(cos(x) - x ^ 3),
#'                                     x1 = 0, x2 = 1)
#' RUnit::checkEqualsNumeric(result, expectation)
regula_falsi <- function(f, x1, x2, epsx = .Machine[["double.eps"]],
                         epsf = epsx,
                         improvement =  c("pegasus", "illinois",
                                        "anderson_bjoerck", "none")) {
    checkmate::assert_numeric(x1, len = 1)
    checkmate::assert_numeric(x2, len = 1)
    checkmate::assert_numeric(epsx, len = 1)
    checkmate::assert_numeric(epsf, len = 1)
    checkmate::assert_function(f, nargs = 1)
    checkmate::assert_character(improvement)
    if (f(x1) * f(x2) > 0) {
        msg <- paste("f(x1) and f(x2) have same sign.",
                      "Choose x1, x2 such that signs differ.")
        throw(msg)
    }
    improvement <- match.arg(improvement)
    if (improvement == "none") {
        result <- .regula_falsi(f = f, a = x1, b = x2, tolerance = epsf)
    } else {
    result <- .better_false_position(x1 = x1, x2 = x2, f1 = f(x1), f2 = f(x2),
                                     f = f, epsx = epsx, epsf = epsf,
                                     improvement = improvement)
    }
    return(result)

}
