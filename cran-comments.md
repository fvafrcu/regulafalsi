Dear CRAN Team,
this is a resubmission of package 'regulaFalsi'. I have added the following changes:

* FIXME

Please upload to CRAN.
Best, Andreas Dominik

# Package regulaFalsi 1.0.0.9000

Reporting is done by packager version 1.8.0.9010


## Test environments
- R Under development (unstable) (2020-12-21 r79668)
   Platform: x86_64-pc-linux-gnu (64-bit)
   Running under: Devuan GNU/Linux 3 (beowulf)
   0 errors | 1 warning  | 1 note 
- gitlab.com
  R Under development (unstable) (2020-12-21 r79668)
  Platform: x86_64-pc-linux-gnu (64-bit)
  Running under: Devuan GNU/Linux 3 (beowulf)
  FAILED

- win-builder (devel)

## Local test results
- RUnit:
    regulaFalsi_unit_test - 2 test functions, 0 errors, 0 failures in 7 checks.
- Testthat:
    
- Coverage by covr:
    regulaFalsi Coverage: 98.11%

## Local meta results
- Cyclocomp:
     no issues.
- lintr:
    found 3 lints in 135 lines of code (a ratio of 0.0222).
- cleanr:
    found 1 dreadful things about your code.
- codetools::checkUsagePackage:
    found 3 issues.
- devtools::spell_check:
    found 11 unkown words.
